#======================
# RAFAŁ PILECKI pr41479
# Zadanie z lab 4
# Na ocene 3
#======================

import sys
import os
import string
import operator

class Program:

    kryteria = ['q','w','e','r','t','y','u','i','o','p','a','s','d','f','g','h','j','k','l','z','x','c','v','b','n','m','Q','W','E','R','T','Y','U','I','O','P','A','S','D','F','G','H','J','K','L','Z','X','C','V','B','N','M']
    histogram = {}
    temp = 0

    def __init__(self):
        self.loadFile()
        self.readFile()
        self.printHistogram()

    def loadFile(self):
        f = open(sys.argv[1], "r")
        self.text = f.read()
    
    def readFile(self):
        for znak in self.text:
            if znak in self.kryteria:
                if self.histogram: 
                    if self.histogram.get(str(znak)):
                        self.histogram[str(znak)] = 1 + self.histogram.get(str(znak))
                        self.temp = self.temp + 1
                    else:
                        self.histogram[str(znak)] = 1
                        self.temp = self.temp + 1 
                else:
                    self.histogram[str(znak)] = 1
                    self.temp = self.temp + 1
    
    def printHistogram(self):
        for key, value in sorted(self.histogram.items(), key=lambda item: item[1], reverse = True):
            f = float(value)/float(self.temp)*100
            print("{} {} {:.2f}%".format(key, value, f))

main = Program()