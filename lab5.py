#======================
# RAFAŁ PILECKI pr41479
# Zadanie z lab 5
# Na ocene 5
#======================
import sys
import os
from collections import defaultdict 

SYSTEM = "/"


class Program:

    files = defaultdict(list) 

    def __init__(self):
        self.researchArgv()
        self.readFile()

    def researchArgv(self):
        p_str = "# findduplicates "
        for path in sys.argv:
            try:
                self.researchDictionary(path)
                p_str = p_str + path + " "
            except OSError:
                pass
        print(p_str)

    def researchDictionary(self, path):
        self.temp = list(os.listdir(path))
        for fileName in self.temp:
            test = path + SYSTEM + fileName
            if os.path.isfile(test):
                self.files[os.path.getsize(test)].append(test)
            else:
                self.researchDictionary(test)

    def readFile(self):
        text = []
        for byte, paths in self.files.items():
            if len(paths) != 1:
                for path in paths:
                    f = open(path, "r")
                    text.append(f.read())
                flag = True
                first = text[0]
                for t in text:
                    if(t != first):
                        flag = False
                if flag:
                    self.printFinal(byte, paths)

    def printFinal(self, byte, paths):
        b_str = "duplicated: (size: " + str(byte) + "b)"  
        print(b_str)
        for path in paths:
            print(path)

main = Program()