#======================
# RAFAŁ PILECKI pr41479
# Zadanie z lab 6 - wersja lab 1
# Na ocene 4
#======================
import sys
import os
import time
from time import strftime, gmtime
from stat import *

SYSTEM = "/"

class Program:

    flagl = 0
    flagL = 0

    path = os.getcwd()

    def __init__(self):
        self.makeFlag()
        self.makeList()
        
    def makeFlag(self):
        k = 0
        for arg in sys.argv:
            if k==0:
                k=1
            else:
                if arg == "-l":
                    self.flagl = 1
                elif arg == "-L":
                    self.flagL = 1
                else:
                    self.path = arg
    
    def makeList(self):
        try:
            files = list(os.listdir(self.path))
            self.printList(files)
        except OSError:
            pass

    def printList(self, files):
        for file in files:
            line = ""
            if self.flagl == 1:
                mode = os.stat(self.path + SYSTEM + file)
                line = line + self.modePrint(mode, mode.st_mode) + "\t"
                line = line + strftime("%Y-%m-%d %H:%M:%S", time.gmtime(os.path.getmtime(self.path + SYSTEM + file))) + "\t"
                line = line + str(os.path.getsize(self.path + SYSTEM + file)) + "\t"
            if self.flagL == 1:
                pass
            line = line + file      
            print(line)

    def modePrint(self,stat, mode):
        result = "" 

        if S_ISDIR(mode):
            result = result + "d" 
        else:
            result = result + "-"

        if mode & S_IRUSR:
            result = result + "r"
        else:
            result = result + "-"
        if mode & S_IWUSR:
            result = result + "w"
        else:
            result = result + "-"
        if mode & S_IXUSR:
            result = result + "x"
        else:
            result = result + "-"

        if mode & S_IRGRP:
            result = result + "r"
        else:
            result = result + "-"
        if mode & S_IWGRP:
            result = result + "w"
        else:
            result = result + "-"
        if mode & S_IXGRP:
            result = result + "x"
        else:
            result = result + "-"
        
        if mode & S_IROTH:
            result = result + "r"
        else:
            result = result + "-"
        if mode & S_IWOTH:
            result = result + "w"
        else:
            result = result + "-"
        if mode & S_IXOTH:
            result = result + "x"
        else:
            result = result + "-"

        return result
    

main = Program()